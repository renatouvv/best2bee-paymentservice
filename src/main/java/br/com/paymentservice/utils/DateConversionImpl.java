package br.com.paymentservice.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DateConversionImpl implements DateConversion {
	
	private static final Logger logger = LoggerFactory.getLogger(DateConversionImpl.class);
	
	public Date convertStringToDate(String stringDate) {
		Date date = null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
			date = formatter.parse(stringDate);
		} catch (Exception e) {
			logger.error("Ocorreu um erro ao realizar a conversão de string para date.", e.getMessage());
		}
		
		return date;
	}

}
