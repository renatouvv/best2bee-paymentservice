package br.com.paymentservice.utils;

import java.util.Date;

public interface DateConversion {
	
	public Date convertStringToDate(String stringDate);

}