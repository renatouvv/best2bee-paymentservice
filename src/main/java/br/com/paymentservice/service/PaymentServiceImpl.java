package br.com.paymentservice.service;

import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.paymentservice.dto.PaymentDetails;
import br.com.paymentservice.utils.DateConversion;

@Service
public class PaymentServiceImpl implements PaymentService {
	
	private static int creditCardNumberSize = 16;
	private static int creditCardNumberValidationSize = 3;
	
	@Autowired
	private DateConversion dateConversion;
	
	private static final Logger logger = LoggerFactory.getLogger(PaymentServiceImpl.class);

	@Override
	public boolean processPayment(PaymentDetails paymentDetails) {
		boolean result = false;
		
		Date date = dateConversion.convertStringToDate(paymentDetails.getExpirationDate());
		Date today = Calendar.getInstance().getTime();
		
		if (paymentDetails.getCardNumber().isEmpty() || paymentDetails.getCardNumber().length() != creditCardNumberSize) {
			logger.info("Número de cartão de crédito inválido.");
		} else if (paymentDetails.getCvv().length() != creditCardNumberValidationSize) {
			logger.info("CVV inválido.");
		} else if (null == date || date.before(today)) {
			logger.info("Cartão de crédito com data de validade vencida.");
		} else {
			result = true;
		}
		
		return result;
	}

}
