package br.com.paymentservice.service;

import br.com.paymentservice.dto.PaymentDetails;

public interface PaymentService {
	
	public boolean processPayment(PaymentDetails paymentDetails);

}