package br.com.paymentservice;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import br.com.paymentservice.controller.PaymentController;

@SpringBootApplication
public class PaymentServiceApplication {
	
	@Bean
	ResourceConfig resourceConfig() {
		return new ResourceConfig().register(PaymentController.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(PaymentServiceApplication.class, args);
	}

}
