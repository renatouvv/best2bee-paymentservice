package br.com.paymentservice.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.paymentservice.dto.PaymentDetails;
import br.com.paymentservice.service.PaymentService;

@Controller
@Path("/payment")
public class PaymentController {
	
	@Autowired
	PaymentService service;

	private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);
	
	@Path("/process")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<String> processPayment(String jsonRequest) {
		
		ObjectMapper mapper = new ObjectMapper();
		PaymentDetails payment;
		boolean dataValidation;
		
		String responseMsg = null;
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		
		try {
			payment = mapper.readValue(jsonRequest, PaymentDetails.class);
			dataValidation = service.processPayment(payment);
			
			if (dataValidation) {
				responseMsg = "Pagamento realizado com sucesso.";
				status = HttpStatus.OK;
				
			} else {
				responseMsg = "Não foi possível processar o pagamento.";
			}
			
			
		} catch (Exception e) {
			logger.error("Ocorreu um erro ao tentar processar o pagamento do pedido." + "\n Mensagem: " + e.getMessage()
					+ "\n Causa: " + e.getCause());
		}
		

		return new ResponseEntity<String>(responseMsg, status);
	}
	
}